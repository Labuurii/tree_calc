function findMatchingDiameterPrice(treeQuality, centimeter) {
	for(const [idx, priceData] of treeQuality.prices.entries()) {
		if(priceData.centimeter > centimeter) {
			if(idx == 0) return priceData
			return treeQuality.prices[idx - 1]
		}
	}
	return treeQuality.prices[treeQuality.prices.length - 1]
}

function findMatchingLengthAdjustment(tree, lengthDec) {
	if(tree.lengthAdjustments.length == 0) return 0;
	
	for(const [idx, lengthData] of tree.lengthAdjustments.entries()) {
		if(lengthData.decimeter > lengthDec) {
			if(idx == 0) return lengthData.adjustment
			return tree.lengthAdjustments[idx - 1].adjustment
		}
	}
	return tree.lengthAdjustments[tree.lengthAdjustments.length - 1].adjustment
}

function calcPrice(tree, treeQuality, diameterCentimeters, lengthDecimeter) {
	const m3Price = findMatchingDiameterPrice(treeQuality, diameterCentimeters).priceM3
	const radius = (diameterCentimeters / 2)
	const area = radius * radius * Math.PI
	const volumeC3 = area * ( lengthDecimeter * 10 )
	const volumeM3 = ( volumeC3 * 0.000001 ) * 1.16
	
	const rawPrice = m3Price * volumeM3
	const lengthAdjustment = findMatchingLengthAdjustment(tree, lengthDecimeter)
	return rawPrice + lengthAdjustment
}

function findStartLength(treeData) {
	<!-- var result = null -->
	<!-- for(treeType of treeData) { -->
		<!-- for(lengthData of treeType.lengthAdjustments) { -->
			<!-- if(!result || lengthData.decimeter < result) -->
				<!-- result = lengthData.decimeter -->
		<!-- } -->
	<!-- } -->
	<!-- return result -->
	
	return 31
}

function findEndLength(treeData) {
	<!-- var result = null -->
	<!-- for(treeType of treeData) { -->
		<!-- for(lengthData of treeType.lengthAdjustments) { -->
			<!-- if(!result || lengthData.decimeter > result) -->
				<!-- result = lengthData.decimeter -->
		<!-- } -->
	<!-- } -->
	<!-- return result -->
	
	return 55
}