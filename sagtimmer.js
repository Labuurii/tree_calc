TREE_PRICES.push({
	name: "Sågtimmer Tall",
	qualities: [
		{
			name: "Stamblock",
			prices: [
				{ centimeter: 24, priceM3: 1100, },
				{ centimeter: 28, priceM3: 1200, },
			]
		},
		{
			name: "Kval 1",
			prices: [
				{ centimeter: 14, priceM3: 550, },
				{ centimeter: 18, priceM3: 725, },
				{ centimeter: 24, priceM3: 825, },
				{ centimeter: 28, priceM3: 925, },
			]
		},
		{
			name: "Kval 2-3",
			prices: [
				{ centimeter: 14, priceM3: 550, },
				{ centimeter: 18, priceM3: 675, },
				{ centimeter: 24, priceM3: 700, },
				{ centimeter: 28, priceM3: 750, },
			]
		},
		{
			name: "Kval 4",
			prices: [
				{ centimeter: 14, priceM3: 550, },
				{ centimeter: 18, priceM3: 625, },
				{ centimeter: 24, priceM3: 625, },
				{ centimeter: 28, priceM3: 625, },
			]
		},
		{
			name: "Kubb",
			prices: [
				{ centimeter: 14, priceM3: 550, },
				{ centimeter: 18, priceM3: 675, },
			]
		},
		{
			name: "Svarvtimmer",
			prices: [
				{ centimeter: 28, priceM3: 775, },
			]
		},
	],
	lengthAdjustments: []
})

TREE_PRICES.push({
	name: "Sågtimmer Gran",
	qualities: [
		{
			name: "Stamblock",
			prices: [
				{ centimeter: 24, priceM3: 900, },
				{ centimeter: 28, priceM3: 1000, },
			]
		},
		{
			name: "Kval 1-2",
			prices: [
				{ centimeter: 14, priceM3: 550, },
				{ centimeter: 18, priceM3: 700, },
				{ centimeter: 24, priceM3: 750, },
				{ centimeter: 28, priceM3: 825, },
			]
		},
		{
			name: "Kubb",
			prices: [
				{ centimeter: 14, priceM3: 550, },
				{ centimeter: 18, priceM3: 700, },
			]
		},
		{
			name: "Svarvtimmer",
			prices: [
				{ centimeter: 28, priceM3: 875, },
			]
		},
	],
	lengthAdjustments: []
})

TREE_PRICES.push({
	name: "Massaved",
	qualities: [
		{
			name: "Barr",
			prices: [
				{ centimeter: 5, priceM3: 465, },
			]
		},
		{
			name: "Björk",
			prices: [
				{ centimeter: 5, priceM3: 505, },
			]
		},
		{
			name: "Asp",
			prices: [
				{ centimeter: 5, priceM3: 505, },
			]
		},
	],
	lengthAdjustments: []
})

TREE_PRICES.push({
	name: "Bränsleved",
	qualities: [
		{
			name: "Ved",
			prices: [
				{ centimeter: 3, priceM3: 480, },
			]
		},
	],
	lengthAdjustments: []
})